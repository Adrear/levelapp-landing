function sendMail(event) {
  document.getElementById("button2").setAttribute("class", "active")
  if (document.getElementById("email").value !== "") {
    var email = document.getElementById("email").value
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    if(validateEmail(email)){
      var dataSend = {
        email_address: email,
        status: 'subscribed'
      }

      event.preventDefault()
      fetch('/api/mail', {
        method: 'post',
        body: JSON.stringify(dataSend),
        headers: {'Content-Type': 'application/json'}
      }).then((response) => {
        if (response.status === 200) successSend()
        if (response.status === 400) errorSend()
      })
        .catch(alert)

      function successSend() {
        document.getElementById("email").value = "";
        document.getElementById("thank").setAttribute("class", "active")
        document.getElementById("form").setAttribute("class", "hidden")
      }
      function errorSend() {
        document.getElementById("exist").setAttribute("class", "active")
      }
    }else {
      console.log("Error Email")
      document.getElementById("email").setAttribute("class", "error")
    }
  } else {
    console.log("Missing Email")
    document.getElementById("email").setAttribute("class", "error")
  }
}

